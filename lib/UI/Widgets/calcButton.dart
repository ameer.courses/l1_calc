import 'package:flutter/material.dart';


class CalcButton extends StatelessWidget {
  final String? content;
  final GestureTapCallback onTap;

  CalcButton({this.content,required this.onTap});



  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          width: double.infinity,
          height: size.width*0.2,
          margin: EdgeInsets.all(size.width*0.025),
          padding: EdgeInsets.all(size.width*0.05),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(size.width*0.025),
            color: theme.primaryColor,
          ),
          child: FittedBox(
              child: Text(content ?? '?')
          ),
        ),
      ),
    );
  }
}
