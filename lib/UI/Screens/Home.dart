import 'package:calc/UI/Widgets/calcButton.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: OrientationBuilder(
          builder: (context,or){
            if(or == Orientation.landscape)
              return Center(
                child: Text('landscape'),
              );

              return Portrait();
          },
        ),
      ),
    );
  }
}


class Portrait extends StatelessWidget {

  final List<List<String>> buttons = [
    ['=','DEL'],
    ['7','8','9','+'],
    ['4','5','6','-'],
    ['1','2','3','×'],
    ['C','0','.','÷'],
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      //crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
            child: Container(
              width: double.infinity,
              height: double.infinity,
              child: Column(
                children: [
                  Expanded(
                      child: SizedBox(
                        width: double.infinity,
                        child: FittedBox(
                            child: Text('125+'),
                          alignment: Alignment.centerLeft,
                        ),
                      )
                  ),
                  Expanded(
                    flex: 2,
                      child: SizedBox(
                        width: double.infinity,
                        child: FittedBox(
                            child: Text('45'),
                          alignment: Alignment.centerLeft,
                        ),
                      )
                  )
                ],
              ),
            )
        ),
        for(int i = 0 ; i < buttons.length ; i++)
          Row(
            children: [
              for(int j = 0 ; j < buttons[i].length ; j++)
                CalcButton(
                  content: buttons[i][j],
                  onTap: (){
                    print(buttons[i][j]);
                  },
                )
            ],
          )
      ],
    );
  }
}

